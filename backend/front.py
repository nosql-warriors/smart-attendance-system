import streamlit as st
import os
import cv2
import time
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import face_recognition
import seaborn as snsstreamlit
# Page layout settings
st.set_page_config(
    page_title="Smart Attendance System",
    page_icon=":books:",
    layout="wide"
)

# Main title and description
st.title("Welcome to Smart Attendance System")
st.write("Explore the features of our Smart Attendance System below.")

# Sidebar navigation
st.sidebar.title("Navigation")

# Assuming you have a way to identify the user type (professor or student)
# Choose user type (Professor or Student)
user_type = st.radio("Select your role:", ["Professor", "Student"])

# Initialize session state
session_state = st.session_state
if "selected_page_professor" not in session_state:
    session_state.selected_page_professor = "Home"
if "professor_logged_in" not in session_state:
    session_state.professor_logged_in = False
if "student_logged_in" not in session_state:
    session_state.student_logged_in = False

def professor_login():
    if session_state.professor_logged_in:
        return True

    st.subheader("Professor Login")
    username = st.text_input("Username:")
    password = st.text_input("Password:", type="password", key="prof_password")
    login_button = st.button("Login")

    # Add your professor login logic here
    if login_button:
        if username == "nikhil" and password == "12345":
            st.success("Login successful! Welcome, Professor.")
            session_state.professor_logged_in = True
            session_state.student_logged_in = False  # Ensure student is not logged in
            return True
        else:
            st.error("Invalid credentials. Please try again.")
    return False

def reset_login_state():
    session_state.professor_logged_in = False
    session_state.student_logged_in = False

def find_encodings(images):
    """
    Find face encodings for a list of images.

    :param images: List of images in BGR format.
    :return: List of face encodings.
    """
    encode_list = []
    for img in images:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        encode = face_recognition.face_encodings(img)[0]
        encode_list.append(encode)
    return encode_list

def recognize_and_mark_attendance(images, class_names):
    """
    Recognize faces from webcam feed and mark attendance.

    :param images: List of known images for face recognition.
    :param class_names: List of corresponding class names.
    """
    # Find face encodings for known images
    encode_list_known = find_encodings(images)
    cap = cv2.VideoCapture(0)

    while True:
        success, img = cap.read()
        img_s = cv2.resize(img, (0, 0), None, 0.25, 0.25)
        img_s = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        faces_cur_frame = face_recognition.face_locations(img_s)
        encodes_cur_frame = face_recognition.face_encodings(img_s, faces_cur_frame)

        for encode_face, face_loc in zip(encodes_cur_frame, faces_cur_frame):
            matches = face_recognition.compare_faces(encode_list_known, encode_face)
            face_dis = face_recognition.face_distance(encode_list_known, encode_face)
            match_index = np.argmin(face_dis)

            if matches[match_index]:
                name = class_names[match_index].upper()
                y1, x2, y2, x1 = face_loc
                cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 2)
                cv2.rectangle(img, (x1, y2 - 35), (x2, y2), (0, 255, 0), cv2.FILLED)
                cv2.putText(img, name, (x1 + 6, y2 - 6), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 2)
                mark_attendance(name)

        cv2.imshow('Webcam', img)
        ch = cv2.waitKey(1)
        if ch & 0XFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
def mark_attendance(name):
    """
    Mark attendance in a CSV file.

    :param name: Name of the recognized person.
    """
    with open('Attendance.csv', 'r+') as file:
        data_list = file.readlines()
        name_list = []
        date_list = []

        for line in data_list:
            entry = line.split(',')
            name_list.append(entry[0])
            if name in entry[0]:
                date_list.append(entry[1])
        date_str = time.strftime("%m/%d/%Y")

        if name in name_list:
            if date_str not in date_list:
                now = datetime.now()
                time_str = now.strftime('%H:%M:%S')
                file.writelines(f'\n{name},{date_str},{time_str}')
        if name not in name_list:
            now = datetime.now()
            time_str = now.strftime('%H:%M:%S')
            file.writelines(f'\n{name},{date_str},{time_str}')
def take_attendance():
    path = r'C:/Users/User/IdeaProjects/smart-attendance-system/backend/ImageAttendance'  # file Path
    imges = []
    classNames = []
    myList = os.listdir(path)
    for cl in myList:
        crtImg = cv2.imread(f'{path}/{cl}')
        imges.append(crtImg)
        classNames.append(os.path.splitext(cl)[0])
    recognize_and_mark_attendance(imges, classNames)



def add_student():
    cam = cv2.VideoCapture(0)  # open camera
    #cv2.namedWindow("Add Student")  # window name
    directory = r'C:/Users/User/IdeaProjects/smart-attendance-system/backend/ImageAttendance'  # directory
    os.chdir(directory)  # Change the current directory to specified directory
    print(os.listdir(directory))
    while True:
        ret, frame = cam.read()  # reading from the camera
        if not ret:
            print("failed to grab frame")
            break

        cv2.imshow("Student", frame)
        ch = cv2.waitKey(1)
        # this block is to close the camera
        if ch & 0XFF == ord('q'):  # if q is pressed by admin
            break
        # this block is to take picture
        elif ch & 0XFF == ord(' '):  # if space is pressed by user
            name = input("Enter name: ")
            img_name = "{}.jpg".format(name)
            cv2.imwrite(img_name, frame)
            print("Screenshot Taken")
    cam.release()
    cv2.destroyAllWindows()
# Run the add_student function

# Run the add_student function
def remove_student():
    st.write("remove student")

import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2

def group_analysis():
    df_all = pd.read_csv("Attendance.csv", parse_dates=['Date'])
    df_all.insert(len(df_all.columns), 'Present', 1)
    df = df_all[['Name', 'Present']]
    df = df.groupby(df.columns.tolist(), as_index=False).size()

    # Set Seaborn style
    sns.set(style="whitegrid")

    # Plotting scatter plot for group analysis
    fig_group = plt.figure(figsize=(20, 8))
    sns.scatterplot(x='Name', y='size', data=df, palette='viridis')
    plt.title("Class Attendance")
    plt.xlabel("Name", fontsize=14)
    plt.ylabel("Number of Occurrences", fontsize=14)
    plt.xticks(rotation=45, fontsize=12)
    plt.yticks(fontsize=12)

    # Display the figure in Streamlit
    st.pyplot(fig_group)

    # Plotting bar chart for total number of students presents
    fig_daily = plt.figure(figsize=(20, 8))
    daily_attendance = df_all[['Date', 'Present']]
    daily_attendance = daily_attendance.groupby(daily_attendance.columns.tolist(), as_index=False).size()
    sns.barplot(x='Date', y='size', data=daily_attendance, palette='viridis')
    plt.title("Total number of students presents")
    plt.xlabel("Date", fontsize=14)
    plt.ylabel("Number of Students", fontsize=14)
    plt.xticks(rotation=45, fontsize=12)
    plt.yticks(fontsize=12)

    # Display the figure in Streamlit
    st.pyplot(fig_daily)

import streamlit as st
import pandas as pd
import cv2
import numpy as np

def individual_analysis(choice):
# Disable the PyplotGlobalUseWarning
    st.set_option('deprecation.showPyplotGlobalUse', False)
    df_attendance = pd.read_csv("Attendance.csv", parse_dates=['Date'])
    names_of_students = df_attendance['Name'].unique().tolist()
    st.write("Inside the function here",names_of_students)
    st.write("Inside the function here3",choice)
    if choice in names_of_students:
        st.write("Inside the function heree")
        df_attendance_name = df_attendance[df_attendance["Name"] == choice]
        draw_graph_for_student(choice, df_attendance_name)

        # Create single person analysis
        if choice in names_of_students:
            st.write("Inside the function hereeee")

# Read the image
    choice = choice.upper()  # Capitalize the first letter of each word

    image = cv2.imread("ImageAttendance/{}.jpg".format(choice))

# Check if the image is loaded successfully
    if image is not None:
        width, height = 450, 550
        dim = (width, height)
        image_half = cv2.resize(image, dim)
        b, g, r = cv2.split(image_half)
        height, width, channels = image_half.shape
        new_window = np.empty([height, width * 2, 3], 'uint8')
        new_window[:, 0:width] = cv2.merge([b, g, r])
        font = cv2.FONT_HERSHEY_SIMPLEX
        no_of_present = len(df_attendance_name)
        no_of_present = str(no_of_present)
        text = "Number of present days: " + no_of_present
        cv2.putText(new_window, choice, (width, 50), font, 1, (0, 255, 255))
        cv2.putText(new_window, text, (width, 100), font, 1, (0, 255, 255))

        # Read second image
        second_image = cv2.imread("Student_Analysis/{}.jpg".format(choice))
        second_dim = 450, 400
        add_image = cv2.resize(second_image, second_dim)
        b, g, r = cv2.split(add_image)
        new_window[150:, width:width * 2] = cv2.merge([b, g, r])

        # Display and save image
        st.image(new_window, channels="BGR", use_column_width=True, caption="Individual Analysis")
    else:
        st.write("Error: Unable to load the image.")
def draw_graph_for_student(name, df):
    df.insert(len(df.columns), 'Present', 1)
    df = df.set_index(df['Date'])
    daily_attendance = df.Present.resample('D').asfreq().fillna(0)

    plt.xticks(rotation=20)
    plt.plot(daily_attendance)

    # Display the figure in Streamlit
    st.pyplot()

# Sample usage in Streamlit app

def professor_panel():
    st.subheader("Professor Panel")

    # Use buttons instead of st.write for options
    if st.button("Take Attendance"):
        session_state.selected_page_professor = "Take Attendance"

    if st.button("Add Student"):
        session_state.selected_page_professor = "Add Student"

    if st.button("Remove Student"):
        session_state.selected_page_professor = "Remove Student"

    if st.button("Group Analysis"):
        session_state.selected_page_professor = "Group Analysis"

    if st.button("Individual Analysis"):
        session_state.selected_page_professor = "Individual Analysis"

# Display content based on user type
if user_type == "Professor":
    professor_login()

    if session_state.professor_logged_in:
        professor_panel()

def individual():
    st.subheader("Individual Analysis")
    # Text input for student to enter their name for analysis
    student_name = st.text_input("Enter your name:")

    # Button to trigger analysis
    if st.button("Analyze"):
        if not student_name:
            st.warning("Please enter your name.")
        else:
            # Add your individual analysis logic here using the entered student_name
            individual_analysis(student_name)
            st.success(f"Analysis for {student_name} completed.")

# Display content based on the selected page
if user_type == "Professor" and session_state.professor_logged_in:
    if session_state.selected_page_professor == "Home":
        st.write("Your home content here.")
    elif session_state.selected_page_professor == "Take Attendance":
        take_attendance()
    elif session_state.selected_page_professor == "Add Student":
        add_student()
    elif session_state.selected_page_professor == "Remove Student":
        remove_student()
    elif session_state.selected_page_professor == "Group Analysis":
        group_analysis()
    elif session_state.selected_page_professor == "Individual Analysis":
        individual()
elif user_type == "Student":
    st.subheader("Student Panel")

    if st.button("Group Analysis"):
        st.write("inside group analysis for student")

    if st.button("Individual Analysis"):
        individual()
