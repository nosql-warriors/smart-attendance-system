import tkinter as tk
from tkinter import ttk, filedialog
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import pandas as pd
import cv2
import numpy as np

def individual_analysis(choice):
    # Disable the PyplotGlobalUseWarning
    df_attendance = pd.read_csv("Attendance.csv", parse_dates=['Date'])
    names_of_students = df_attendance['Name'].unique().tolist()

    if choice in names_of_students:
        df_attendance_name = df_attendance[df_attendance["Name"] == choice]
        draw_graph_for_student(choice, df_attendance_name)

        # Create single person analysis
        if choice in names_of_students:
            # Read the image
            choice = choice.upper()  # Capitalize the first letter of each word
            image = cv2.imread("ImageAttendance/{}.jpg".format(choice))

            # Check if the image is loaded successfully
            if image is not None:
                width, height = 450, 550
                dim = (width, height)
                image_half = cv2.resize(image, dim)
                b, g, r = cv2.split(image_half)
                height, width, channels = image_half.shape
                new_window = np.empty([height, width * 2, 3], 'uint8')
                new_window[:, 0:width] = cv2.merge([b, g, r])
                font = cv2.FONT_HERSHEY_SIMPLEX
                no_of_present = len(df_attendance_name)
                no_of_present = str(no_of_present)
                text = "Number of present days: " + no_of_present
                cv2.putText(new_window, choice, (width, 50), font, 1, (0, 255, 255))
                cv2.putText(new_window, text, (width, 100), font, 1, (0, 255, 255))

                # Read second image
                second_image = cv2.imread("Student_Analysis/{}.jpg".format(choice))
                second_dim = 450, 400
                add_image = cv2.resize(second_image, second_dim)
                b, g, r = cv2.split(add_image)
                new_window[150:, width:width * 2] = cv2.merge([b, g, r])

                # Display and save image
                plt.imshow(cv2.cvtColor(new_window, cv2.COLOR_BGR2RGB))
                plt.axis('off')
                plt.title("Individual Analysis")
                plt.show()
            else:
                print("Error: Unable to load the image.")
    else:
        print("Error: Choice not found in the list.")

def draw_graph_for_student(name, df):
    df.insert(len(df.columns), 'Present', 1)
    df = df.set_index(df['Date'])
    daily_attendance = df.Present.resample('D').asfreq().fillna(0)

    plt.xticks(rotation=20)
    plt.plot(daily_attendance)
    plt.title(f"Attendance Analysis for {name}")
    plt.xlabel("Date")
    plt.ylabel("Present (1) / Absent (0)")
    plt.show()

# Example usage
root = tk.Tk()
root.title("Individual Analysis Example")

# Create a button to trigger individual analysis
button = tk.Button(root, text="Perform Individual Analysis", command=lambda: individual_analysis("John Doe"))
button.pack(pady=20)

root.mainloop()
