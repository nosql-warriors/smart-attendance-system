# importing all the classes and methods which are present inside tkinter

from tkinter import *
from tkinter import messagebox
from DatabaseHandler import DatabaseHandler
from PIL import ImageTk  # PIL Python Image Library To import jpg images
db =  DatabaseHandler()
# # for creating window we have a class inside this module that is tk() class
# tk() class helps us to create a UI


def login():
    if usernameEntry.get()=='' or passwordEntry.get()=='':
        messagebox.showerror('Error','Fields cannot be empty')
    professor = db.professors_collection.find_one({'username': usernameEntry.get()})
    if professor and professor['password'] == db.hash_password(passwordEntry.get()):
        messagebox.showinfo('Success', 'Welcome, Enlight The World Now ')
        windows.destroy()
        import Professor_analysis
    else:
        messagebox.showerror('Error', 'Fool Enter Correct Credential')

windows = Tk()
windows.title('Smart Attendance Login System')

# we are using one of the method present inside this Tk() class
# mainloop() simply keep our window on a loop to see it continuously
# to make our window visible
windows.geometry('1280x700+0+0')  # width and height +0 is
#windows.resizable(False, False)

# background_image = ImageTk.PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/last.png')

# bgLabel = Label(windows,image=background_image)
# bgLabel.place(x=0, y=0)

loginFrame = Frame(windows,)
loginFrame.place(x=400, y=150)

logoImage = PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/professor.png')
logoLabel = Label(loginFrame, image=logoImage)
logoLabel.grid(row=0, column=0, columnspan=2, pady=10)


# Username
usernameImage = PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/user.png')
usernameLabel = Label(loginFrame, image=usernameImage, text='Username', compound=LEFT,
                      font=('times new roman', 20, 'bold'))
usernameLabel.grid(row=1, column=0, pady=10, padx=20)

usernameEntry = Entry(loginFrame, font=('times new roman', 20, 'bold'), bd=5, fg='royalblue')
usernameEntry.grid(row=1, column=1, pady=10,padx=20)

# password
passwordImage = PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/padlock.png')
passwordLabel = Label(loginFrame, image=passwordImage, text='Password', compound=LEFT,
                      font=('times ew roman', 20, 'bold'))
passwordLabel.grid(row=2, column=0, pady=10, padx=20)

passwordEntry = Entry(loginFrame, font=('times new roman', 20, 'bold'), bd=5, fg='royalblue')
passwordEntry.grid(row=2, column=1, pady=10, padx=20)

# login class using button class

loginButton = Button(loginFrame, text='Login', font=('times new roman', 14, 'bold'), width=15,
                     fg='white', bg ='cornflowerblue', activebackground='cornflowerblue', activeforeground='cornflowerblue',
                     cursor='hand2', command=login)
loginButton.grid(row=3, column=1, pady=10)

windows.mainloop()

