from tkinter import *
from tkinter import messagebox
from DatabaseHandler import DatabaseHandler
from PIL import ImageTk  # PIL Python Image Library To import jpg images
db =  DatabaseHandler()
import tkinter as tk
from tkinter import ttk
import subprocess
import os

def on_role_selection():
    selected_role = role_var.get()
    if selected_role == "Professor":
        subprocess.run(["python", "P_login.py"])


    elif selected_role == "Student":
        subprocess.run(["python", "S_login.py"])


def destroy_current_ui():
    # Destroy all current UI elements
    for widget in root.winfo_children():
        widget.destroy()


# Create the main window
root= Tk()
root.title('Smart Attendance Login System')

# we are using one of the method present inside this Tk() class
# mainloop() simply keep our window on a loop to see it continuously
# to make our window visible
root.geometry('1280x700+0+0')  # width and height +0 is
#windows.resizable(False, False)

#background_image = ImageTk.PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/login.jpg')

#bgLabel = Label(root,image=background_image)
#bgLabel.place(x=0, y=0)

loginFrame = Frame(root)
loginFrame.place(x=400, y=150)


# Main title and description
# Main title and description
title_image = PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/attendance.png')
title_label = ttk.Label(root, image=title_image, text="Welcome to Smart Attendance System",compound=LEFT,  font=('times new roman', 40, 'bold'))

title_label.grid(row=0, column=0, pady=10, columnspan=2)

# Center the label within the window
root.columnconfigure(0, weight=1)


description_label = ttk.Label(root, text="  Explore the features of our Smart Attendance System below", font=('times new roman', 20, 'bold'),)
description_label.grid(row=1, column=0, pady=30, padx=(40, 0), columnspan=2)

# Sidebar navigation
sidebar_frame = ttk.Frame(root)
sidebar_frame.grid(row=2, column=0, pady=10)

sidebar_label = ttk.Label(sidebar_frame, text="Navigation", font=('times new roman', 20, 'bold'))
sidebar_label.grid(row=0, column=0, pady=10)

# Choose user type (Professor or Student)
role_var = tk.StringVar()
#role_var.set("Professor")  # Default selection

# Create a style object
style = ttk.Style()

# Increase the font size for the radio buttons
style.configure("TRadiobutton", font=("Times new Roman", 13))

role_radio_professor = ttk.Radiobutton(sidebar_frame, text="Professor", variable=role_var, value="Professor", style="TRadiobutton")

role_radio_student = ttk.Radiobutton(sidebar_frame, text="Students", variable=role_var, value="Student", style="TRadiobutton")

role_radio_professor.grid(row=1, column=0, pady=5)
role_radio_student.grid(row=2, column=0, pady=5)

# Button to trigger role selection
select_role_button = ttk.Button(sidebar_frame, text="Enter", command=on_role_selection)
select_role_button.grid(row=3, column=0, pady=10)



# Start the main event loop
root.mainloop()
