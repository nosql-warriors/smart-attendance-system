import sqlite3
from tkinter import *
import time
import ttkthemes
from tkinter import ttk,messagebox
import cv2
import os
from datetime import datetime
import numpy as np
import face_recognition
from DatabaseHandler import DatabaseHandler
db = DatabaseHandler()
from pymongo import MongoClient
import pymongo
import pandas as pd
import matplotlib.pyplot as plt
import cv2
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg



# functionality

def remove_student():
    # Simulate the removal process (no actual database connection)
    # This is just for demonstration purposes

    # Assuming a success scenario (adjust as needed for your use case)
    success_scenario = True

    if success_scenario:
        messagebox.showinfo('Success', f'Student with ID  removed successfully!')
    else:
        messagebox.showwarning('Warning', f'No student found with ID.')



def groups_analysis():
    success_scenario = True

    if success_scenario:
        messagebox.showinfo('Success', f'Welcome to the Group Analysis!')
    else:
        messagebox.showwarning('Warning', f'No student found with ID.')



def individuals_analysis():
    success_scenario = True

    if success_scenario:
        messagebox.showinfo('Success', f'Welcome to the Individual Analysis!')
    else:
        messagebox.showwarning('Warning', f'No student found with ID.')

def exit_ui():
    root.destroy()

count=0
text=''

def connect_database():
    def connect():
        try:
            con=sqlite3.connect(host=hostEntry.get(), user=usernameEntry.get(), password=passwordEntry.get())
            mycursor=con.cursor()
            messagebox.showinfo('Success','Welcome Shitty Head')

        except:
            messagebox.showerror('Error', 'Fool Enter valid Credentials or Get Lost', parent=connectingWindow)


    connectingWindow = Toplevel()
    connectingWindow.grab_set()
    connectingWindow.geometry('470x290+730+230')
    connectingWindow.title('Database Connection')
    connectingWindow.resizable(0,0)

    hostnameLabel = Label(connectingWindow, text='Host Name', font=('arial', 20, 'bold'))
    hostnameLabel.grid(row=0, column=0, padx=20)

    hostEntry = Entry(connectingWindow, font= ('roman', 15, 'bold'), bd=2)
    hostEntry.grid(row=0, column=1, padx=40, pady= 20)

    usernameLabel = Label(connectingWindow, text='User Name',  font=('arial', 20, 'bold'))
    usernameLabel.grid(row=1, column=0,  pady= 20)

    usernameEntry = Entry(connectingWindow, font= ('roman', 15, 'bold'), bd=2)
    usernameEntry.grid(row=1, column=1, padx=40, pady= 20)

    passwordLabel = Label(connectingWindow, text='Password',  font=('arial', 20, 'bold'))
    passwordLabel.grid(row=2, column=0,  pady= 20)

    passwordEntry = Entry(connectingWindow, font= ('roman', 15, 'bold'), bd=2)
    passwordEntry.grid(row=2, column=1, padx=40, pady= 20)

    connectButton= ttk.Button(connectingWindow, text = 'CONNECT', command=connect)
    connectButton.grid(row=3, columnspan= 2)






def add_student():
    def add_data():
        if nameEntry.get()=='' or nameLastEntry.get()=='' or DateEntry.get()=='' or TimeEntry.get()=='':
            messagebox.showerror('Error', "All Fields are Required", parent=studentWindow)
        else:
            add_student()


        result=messagebox.askyesno('Confirm','Data added Successfully. Do you want to clean the from?', parent =studentWindow )
        if result:
            nameEntry.delete(0,END)
            nameLastEntry.delete(0,END)
            DateEntry.delete(0,END)
            TimeEntry.delete(0,END)

        else:
            pass



    studentWindow = Toplevel()
    studentWindow.grab_set()
    studentWindow.resizable(False,False)
    namelabel= Label(studentWindow, text='First Name', font=('times new roman', 20, 'bold'))
    namelabel.grid(row=0, column=0, padx=30, pady=15, sticky=W)
    nameEntry = Entry(studentWindow, font=('times new roman', 15, 'bold'), width=24)
    nameEntry.grid(row=0, column=1, pady=15, padx=10)

    nameLastlabel= Label(studentWindow, text='Last Name', font=('times new roman', 20, 'bold'))
    nameLastlabel.grid(row=1, column=0, padx=30, pady=15, sticky=W)
    nameLastEntry = Entry(studentWindow, font=('times new roman', 15, 'bold'), width=24)
    nameLastEntry.grid(row=1, column=1, pady=15, padx=10)

    Datelabel= Label(studentWindow, text='Date', font=('times new roman', 20, 'bold'))
    Datelabel.grid(row=2, column=0, padx=30, pady=15, sticky=W)
    DateEntry = Entry(studentWindow, font=('times new roman', 15, 'bold'), width=24)
    DateEntry.grid(row=2, column=1, pady=15, padx=10)

    Timelabel= Label(studentWindow, text='Time', font=('times new roman', 20, 'bold'))
    Timelabel.grid(row=3, column=0, padx=30, pady=15, sticky=W)
    TimeEntry = Entry(studentWindow, font=('times new roman', 15, 'bold'), width=24)
    TimeEntry.grid(row=3, column=1, pady=15, padx=10)

    add_student_button=Button=ttk.Button(studentWindow, text='ADD STUDENT', command=add_data)
    add_student_button.grid(row=4, columnspan=2, pady=15)


    studentWindow.title('Add Student')
    cam = cv2.VideoCapture(0)  # open camera
    directory = r'C:/Users/User/IdeaProjects/smart-attendance-system/backend/ImageAttendance'  # directory
    os.chdir(directory)  # Change the current directory to specified directory
    print(os.listdir(directory))





    # Treeview for displaying student information
    tree = ttk.Treeview(root, columns=('Name', 'Image'))
    tree.heading('#0', text='ID')
    tree.heading('Name', text='Name')
    tree.heading('Image', text='Image')

    def update_treeview(name, img_name):
        # Insert student info into the Treeview
        tree.insert('', 'end', text=len(tree.get_children()) + 1, values=(name, img_name))

    while True:
        ret, frame = cam.read()  # reading from the camera
        if not ret:
            print("failed to grab frame")
            break

        cv2.imshow("Student", frame)
        ch = cv2.waitKey(1)
        # this block is to close the camera
        if ch & 0xFF == ord('q'):  # if q is pressed by admin
            break
        # this block is to take picture
        elif ch & 0xFF == ord(' '):  # if space is pressed by user
            name = input("Enter name: ")
            img_name = "{}.jpg".format(name)
            cv2.imwrite(img_name, frame)
            print("Screenshot Taken")
            update_treeview(name, img_name)

    cam.release()
    cv2.destroyAllWindows()

def find_encodings(images):
    encode_list = []
    for img in images:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        encode = face_recognition.face_encodings(img)[0]
        encode_list.append(encode)
    return encode_list

def recognize_and_mark_attendance(images, class_names, tree):
    encode_list_known = find_encodings(images)
    cap = cv2.VideoCapture(0)

    while True:
        success, img = cap.read()
        img_s = cv2.resize(img, (0, 0), None, 0.25, 0.25)
        img_s = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        faces_cur_frame = face_recognition.face_locations(img_s)
        encodes_cur_frame = face_recognition.face_encodings(img_s, faces_cur_frame)

        for encode_face, face_loc in zip(encodes_cur_frame, faces_cur_frame):
            matches = face_recognition.compare_faces(encode_list_known, encode_face)
            face_dis = face_recognition.face_distance(encode_list_known, encode_face)
            match_index = np.argmin(face_dis)

            if matches[match_index]:
                name = class_names[match_index].upper()
                y1, x2, y2, x1 = face_loc
                cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 2)
                cv2.rectangle(img, (x1, y2 - 35), (x2, y2), (0, 255, 0), cv2.FILLED)
                cv2.putText(img, name, (x1 + 6, y2 - 6), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 2)
                mark_attendance(name, tree)

        cv2.imshow('Webcam', img)
        ch = cv2.waitKey(1)
        if ch & 0XFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()

def mark_attendance(name, tree):
    db.take_attendance(name)
    with open('Attendance.csv', 'r+') as file:
        data_list = file.readlines()
        name_list = []
        date_list = []

        for line in data_list:
            entry = line.split(',')
            name_list.append(entry[0])
            if name in entry[0]:
                date_list.append(entry[1])
        date_str = time.strftime("%m:%d:%Y")
        

        if name in name_list:
            if date_str not in date_list:
                now = datetime.now()
                time_str = now.strftime('%H:%M:%S')
                file.writelines(f'\n{name},{date_str},{time_str}')
        if name not in name_list:
            now = datetime.now()
            time_str = now.strftime('%H:%M:%S')
            file.writelines(f'\n{name},{date_str},{time_str}')

def take_attendance():
    path = r'C:/Users/User/IdeaProjects/smart-attendance-system/backend/ImageAttendance'
    images = []
    class_names = []
    myList = os.listdir(path)
    for cl in myList:
        crt_img = cv2.imread(f'{path}/{cl}')
        images.append(crt_img)
        class_names.append(os.path.splitext(cl)[0])



    # Create Treeview for displaying attendance
    tree = ttk.Treeview(root, columns=('Name', 'Date', 'Time'))
    tree.heading('#0', text='ID')
    tree.heading('Name', text='Name')
    tree.heading('Date', text='Date')
    tree.heading('Time', text='Time')

    # Recognize and mark attendance
    recognize_and_mark_attendance(images, class_names, tree)
def slider():
    global text, count
    if count == len(s):
        count = 0
        text = ''
    text = text+s[count]
    sliderLabel.config(text=text)
    count += 1
    sliderLabel.after(300,slider)

def clock():
    date = time.strftime('%d/%m/%Y')  # it will provide us the current date
    current_time = time.strftime('%H:%M:%S')
    datetimeLabel.config(text=f' Date: {date}\nTime: {current_time}')  # config = update, f = to concatenate string with variable
    datetimeLabel.after(1000,clock) # after 1 min clock function wii be called and second will be updated

# GUI   part
root=ttkthemes.ThemedTk()  # we can apply themes on buttons



root.get_themes()
root.set_theme('radiance')


root.geometry('1174x680+0+0')
root.resizable(0,0)
root.title('Smart Attendance System')

datetimeLabel = Label(root,text='Hello', font=('times new roman', 18,'bold'))
datetimeLabel.place(x=5,y=5)

clock()

s='Smart Attendance System'  # s[count]=twhen count is 1
sliderLabel = Label(root, text=s, font=('arial', 28, 'italic bold'), width=30)
sliderLabel.place(x=200, y = 0)
slider()

connectButton = ttk.Button(root, text='Connect to Database', command =connect_database )
connectButton.place(x=1000, y=0)

leftFrame = Frame(root)
leftFrame.place(x=50, y=80, width=300, height=600)


logo_image = PhotoImage(file='C:/Users/User/IdeaProjects/smart-attendance-system/Frontend/group.png')
logo_label = Label(leftFrame, image=logo_image)
logo_label.grid(row=2, column=0)

attendanceButton = ttk.Button(leftFrame, text='Take Attendance', width=25, command= take_attendance)
attendanceButton.grid(row=4, column=0, pady=20)

addStudentButton = ttk.Button(leftFrame, text='Add Student', width=25,  command = add_student)
addStudentButton.grid(row=5, column=0, pady=20)

deleteStudentButton = ttk.Button(leftFrame, text='Remove Student', width=25, command=remove_student)
deleteStudentButton.grid(row=6, column=0, pady=20)

groupAnalysisButton = ttk.Button(leftFrame, text='Group Analysis', width=25, command= groups_analysis)
groupAnalysisButton.grid(row=7, column=0, pady=20)

individualAnalysisButton = ttk.Button(leftFrame, text='Individual Analysis', width=25, command=individuals_analysis)
individualAnalysisButton.grid(row=8, column=0, pady=20)


exitButton = ttk.Button(leftFrame, text='Exit', width=25, command=exit_ui)
exitButton.grid(row=9, column=0, pady=20)

rightFrame = Frame(root, bg ='lightgrey')
rightFrame.place(x=350, y=80, width=820, height=600)

exitButton = ttk.Button(leftFrame, text='Individual Analysis', width=25)
exitButton.grid(row=8, column=0, pady=20)


scrollBarX = Scrollbar(rightFrame, orient = HORIZONTAL)
scrollBarY = Scrollbar(rightFrame, orient = VERTICAL)


studentTable = ttk.Treeview(rightFrame, columns=('First_Name', 'Last_Name', 'Date', 'Time'), xscrollcommand=scrollBarX.set, yscrollcommand=scrollBarY.set)

scrollBarX.config(command=studentTable.xview)
scrollBarY.config(command=studentTable.yview)

scrollBarX.pack(side=BOTTOM, fill=X)
scrollBarY.pack(side=RIGHT, fill=Y)

studentTable.pack(fill=BOTH, expand =1 )


studentTable.heading('First_Name', text='First Name')
studentTable.heading('Last_Name', text='Last Name')
studentTable.heading('Date', text='Date')
studentTable.heading('Time', text='Time')


studentTable.config(show='headings')
root.mainloop()