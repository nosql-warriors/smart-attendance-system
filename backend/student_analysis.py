import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2

df_all = pd.read_csv("Attendance.csv", parse_dates=['Date'])
# this function will display the group analysis of the attendance file
def group_analysis(df_all):
    df_all.insert(len(df_all.columns), 'Present', 1)
    df = df_all[['Name', 'Present']]
    df = df.groupby(df.columns.tolist(), as_index=False).size()
    print(df)
    plt.figure(figsize=(13, 5))
    # plt.subplot(131)
    # plt.bar(df['Name'], df['size'])
    plt.xticks(rotation=16)
    # plt.subplot(132)
    plt.scatter(df['Name'], df['size'])
    # plt.xticks(rotation=16)
    # plt.subplot(132)
    # plt.xticks(rotation=16)
    # plt.plot(df['Name'], df['size'])
    # plt.suptitle("Class Attendance")
    plt.title("Class Attendance")
    plt.show()

    # second analysis
    plt.figure(figsize=(10, 7))
    daily_attendance = df_all[['Date', 'Present']]
    print("df_all", daily_attendance)
    daily_attendance = daily_attendance.groupby(daily_attendance.columns.tolist(), as_index=False).size()
    print(daily_attendance)
    plt.xticks(rotation=20)
    plt.xlabel("Date")
    plt.ylabel("No. of students")
    plt.title("Total number of students presents")
    plt.plot(daily_attendance['Date'], daily_attendance['size'])
    plt.show()


# this function will display the 3 subplot of the admin entered name
def draw_graph(name, df):
    df.insert(len(df.columns), 'Present', 1)
    df = df.set_index(df['Date'])
    print(type(df['Date']))
    print(df)
    daily_attendance = df.Present.resample('D').asfreq().fillna(0)
    print("daily_attendance", daily_attendance)
    plt.xticks(rotation=20)
    plt.plot(daily_attendance)
    plt.savefig('Student_Analysis/{}.jpg'.format(name))
    #plt.show()
    # print(df)

def draw_graph_individual(name, df):
    # Assuming df has columns 'Date' and 'Present'
    df.insert(len(df.columns), 'Present', 1)
    df = df.set_index(df['Date'])

    daily_attendance = df.Present.resample('D').asfreq().fillna(0)

    # Plot the attendance graph
    plt.figure(figsize=(16, 12))

    # Display the image on the left side
    img_path = f"ImageAttendance/{name}.jpg"
    img = cv2.imread(img_path)
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img_width, img_height = 400, 500  # Controls the size of the image
    img = cv2.resize(img_rgb, (img_width, img_height))

    plt.subplot(1, 2, 1)
    plt.imshow(img)
    plt.axis('off')  # Hide axis
    plt.title(f"Image of {name}")

    # Display the graph on the right side
    plt.subplot(1, 2, 2)
    plt.plot(daily_attendance, marker='o', linestyle='-', color='b')
    plt.title(f"Attendance Analysis for {name}")
    plt.xlabel("Date")
    plt.ylabel("Present (1) / Absent (0)")
    plt.xticks(rotation=20)
    plt.grid(True)

    # Add text with the number of present days inside the graph
    no_of_present = len(df)
    plt.text(0.5, -0.1, f"Number of Present Days: {no_of_present}", fontsize=22,
             transform=plt.gca().transAxes, color='black', bbox=dict(facecolor='white', alpha=0.7), ha='center')

    plt.tight_layout()
    plt.savefig(f'Student_Analysis/{name}_Analysis.jpg')
    plt.show()

def student_analysis():
    df_attendance = pd.read_csv("Attendance.csv", parse_dates=['Date'])  # read attendance file
    print(df_attendance)
    names_of_student = df_attendance['Name']  # finding the name of the students
    names_of_student = list(set(names_of_student))  # removing the duplicate name
    print(names_of_student)

    print("Do you want Single student Analysis or Group analysis")
    name = input("Press G for Group analysis or Enter Name: ").upper()
    if name == 'G':  # call the function for group analysis
        group_analysis(df_attendance)

    elif name in df_attendance["Name"].values:
        # name = input("Enter the name of the student: ")  # taking the  name from the user
        df_attendance_name = df_attendance.loc[(df_attendance["Name"] == name)]  # take only that row which matches
        # input name with system name
        draw_graph_individual(capitalize_name(name), df_attendance_name)  # call function to display the single person analysis
    else:
        print("Entered wrong input")

def capitalize_name(full_name):
    # Split the full name into first name and last name
    name_parts = full_name.split()

    # Capitalize the first letter of each part
    capitalized_name = ' '.join([part.capitalize() for part in name_parts])

    return capitalized_name