from pymongo import MongoClient
from datetime import datetime
import hashlib
import time
import base64
import bcrypt


class DatabaseHandler:
    DATABASE_NAME = 'attendance_system'
    STUDENTS_COLLECTION_NAME = 'students'
    PROFESSORS_COLLECTION_NAME = 'professors'

    def __init__(self):
        self.client = MongoClient('mongodb+srv://admin:admin@cluster0.jr9a37m.mongodb.net/?retryWrites=true&w=majority')
        self.db = self.client[DatabaseHandler.DATABASE_NAME]
        self.students_collection = self.db[DatabaseHandler.STUDENTS_COLLECTION_NAME]
        self.professors_collection = self.db[DatabaseHandler.PROFESSORS_COLLECTION_NAME]

    def get_all_students(self):
        # Get all students from the database
        return list(self.students_collection.find())





    def take_attendance(self, name):
        name = name.strip()
        # Convert the name to lowercase for case-insensitive matching
        query = {'name': name.lower()}

        # Get the current date and time
        current_datetime = datetime.now()
        current_date = current_datetime.strftime('%Y-%m-%d')  # Format date as string
        current_time = current_datetime.strftime('%H:%M:%S')  # Format time as string

        # Check if there is attendance for today
        existing_attendance = self.students_collection.find_one(query, {'attendance': {'$elemMatch': {'date': current_date}}})

        if existing_attendance:
            # If attendance for today exists, replace it
            self.students_collection.update_one(
                {'_id': existing_attendance['_id'], 'attendance.date': current_date},
                {'$set': {'attendance.$.time': current_time}}
            )
        else:
            # If attendance for today doesn't exist, add new attendance
            new_attendance = {'date': current_date, 'time': current_time}
            self.students_collection.update_one(
                query,
                {'$addToSet': {'attendance': new_attendance}},
                upsert=True
            )

    def get_student_image(self, name):
        student_data = self.students_collection.find_one({'name': name})
        return student_data['image'] if student_data and 'image' in student_data else None

    def remove_student(self, student_id):
        self.students_collection.delete_one({'_id': student_id})


    def get_student_attendance(self, name):
        # Find the student in the database
        student = self.students_collection.find_one({'name': name})

        if student:
            return student.get('attendance', [])
        else:
            print(f"Student {name} not found in the database.")
            return []

    def hash_password(self,password):
        hashed_password = hashlib.sha256(password.encode()).hexdigest()
        return hashed_password

    def create_professor(self):
        username = input("Enter Professor Username: ")
        password = input("Enter Professor Password: ")

        # Hash the password before storing it in the database
        hashed_password = self.hash_password(password)

        # Insert the professor into the database
        self.professors_collection.insert_one({'username': username, 'password': hashed_password})
        print("Professor created successfully.")

    def remove_professor(self, username):
        # Remove a professor from the database
        self.professors_collection.delete_one({'username': username})

    def get_all_professors(self):
        # Get all professors from the database
        return list(self.professors_collection.find())

    def get_professor_encoding(self, username):
        # Get the face encoding of a professor
        professor = self.professors_collection.find_one({'username': username})
        return professor.get('face_encoding', None)
    def check_professor_credentials(self, username, password):
        # Check if the professor exists in the MongoDB collection
        professor = self.professors_collection.find_one({'username': username})

        if professor:
            # Check the hashed password
            hashed_password = professor.get('password', b'')

            if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
                return True  # Passwords match
            else:
                return False  # Passwords do not match
        else:
            return False  # Professor not found
